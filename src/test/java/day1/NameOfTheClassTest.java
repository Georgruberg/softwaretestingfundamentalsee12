package day1;

import org.junit.jupiter.api.*;

class NameOfTheClassTest {

    NameOfTheClass nameOfTheClass;

    @BeforeAll
    static void init() {
        System.out.println("This message will be displayed  once before execution of all testing methods");
    }

    @BeforeEach
    void setup() {
        //given
        nameOfTheClass = new NameOfTheClass();
        System.out.println("before each method runs");
    }

    @AfterEach
    void afterEachMethod() {
        System.out.println("Method is finished...");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After all method runs");
    }


    @Test
    void isNotAdult() {
        boolean isAdult = nameOfTheClass.isAdult(15);
        Assertions.assertFalse(isAdult);
        /*
        * some more operations with object here
        * */
    }

    @Test
    void isAdult() {
        boolean isAdult = nameOfTheClass.isAdult(35);
        Assertions.assertTrue(isAdult);
    }

    @Test
    void isAdultEdgeCase() {
        boolean isAdult = nameOfTheClass.isAdult(17);
        System.out.println(isAdult);
        Assertions.assertFalse(isAdult);
    }

    @Test
    @DisplayName("This method test with parameter value 18")
    void isAdultEdgeCaseFor18() {
        boolean isAdult = nameOfTheClass.isAdult(18);
        System.out.println(isAdult);
        Assertions.assertTrue(isAdult);
    }

}