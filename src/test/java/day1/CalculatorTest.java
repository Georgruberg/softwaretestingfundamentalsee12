package day1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void add() {
        Calculator calculator = new Calculator();
        long actualValue = calculator.add(5, 6);
        Assertions.assertEquals(11, actualValue);
    }

    @Test
    void addTestWithMinusValues() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(0, calculator.add(-5, 5));
    }

    @Test
    void addTestWithTwoMinusValues() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(-10, calculator.add(-5, -5));
    }

    @Test
    void addTestWithIntegerMaxValue() {
        Calculator calculator = new Calculator();
        long result = calculator.add(2147483647, 2147483647);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(4294967294L, result);
        Assertions.assertNull(null);
    }

    @Test
    void arrayTest() {
        Assertions.assertArrayEquals(new int[]{1, 2, 3}, new int[]{1, 2, 3});
    }

    @Test
    void addMethodTest() {
        Calculator calculator = new Calculator();
        long result = calculator.add(5, 5);
        //assume that we need to assert the value between 9 to 11
        //Assertions.assertEquals(10, result);
        Assertions.assertTrue(result > 9);
        Assertions.assertTrue(result < 11);
    }

    @Test
    void addMethodTestWithAssertJ() {
        Calculator calculator = new Calculator();
        long actualResult = calculator.add(6, 7);

        //
        Assertions.assertEquals(13, actualResult);
        org.assertj.core.api.Assertions.assertThat(actualResult).isEqualTo(13);

        //
        Assertions.assertTrue(actualResult > 9);
        org.assertj.core.api.Assertions.assertThat(actualResult).isGreaterThan(9);

        //
        Assertions.assertTrue(actualResult > -1);
        org.assertj.core.api.Assertions.assertThat(actualResult).isNotNegative();

        //
        Assertions.assertTrue(actualResult > 9);
        Assertions.assertTrue(actualResult < 15);

        org.assertj.core.api.Assertions.assertThat(actualResult).isBetween(9L, 15L);



    }

}