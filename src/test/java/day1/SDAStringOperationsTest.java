package day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SDAStringOperationsTest {

    @Test
    void concat() {
        SDAStringOperations operations = new SDAStringOperations();
        Assertions.assertEquals("ab", operations.concat("a", "b"));
    }

    //special chars
    //empty string
    //very long string

    @Test
    void concatTest() {
        SDAStringOperations operations = new SDAStringOperations();
        Assertions.assertEquals("^+%&/()", operations.concat("^+%", "&/()"));
    }

    @Test
    void concatEmptyStringTest() {
        SDAStringOperations operations = new SDAStringOperations();
        Assertions.assertEquals("", operations.concat("", ""));
    }

    //HOMEWORK
    //Test this method with 2 max length string


}