package day1;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SDANumberGeneratorTest {

    @Test
    void getRandomInt() {
        SDANumberGenerator random = new SDANumberGenerator();
        int actualValue = random.getRandomInt(10);

        //Assertions.assertTrue(actualValue > 0);
        //Assertions.assertTrue(actualValue < 10);

        Assertions.assertThat(actualValue).isLessThan(11).isGreaterThan(0).isPositive();
    }
}