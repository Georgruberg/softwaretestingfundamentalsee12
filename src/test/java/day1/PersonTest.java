package day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PersonTest {

    @Test
    void getAge() {
        Person person = new Person("akin", "garagon", 1986, "Developer");
        Assertions.assertEquals(35, person.getAge());
    }

    @Test
    void getAgeWithNullBirthYear() {
        Person person = new Person("akin", "garagon", null, "Developer");
        Assertions.assertEquals(0, person.getAge());
    }

    @Test
    void getFullName() {
        Person person = new Person("akin", "garagon", null, "Developer");
        Assertions.assertEquals("akin garagon",person.getFullName());
    }
}