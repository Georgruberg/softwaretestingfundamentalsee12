package day1;

import java.time.LocalDateTime;

public class Person {
    private String name;
    private String surname;
    private Integer birthYear;
    private String job;

    public Person(String name, String surname, Integer birthYear, String job) {
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        if (birthYear == null) return 0;
        int year = LocalDateTime.now().getYear();
        return year - birthYear;
    }


    public String getFullName() {
        return name + " " + surname;
    }

}
