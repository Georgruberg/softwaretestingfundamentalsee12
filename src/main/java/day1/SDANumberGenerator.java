package day1;

import java.util.Random;

public class SDANumberGenerator {
    public int getRandomInt(int bound) {
        //returns a value between 1 to bound
        Random random = new Random();
        return random.nextInt(bound) + 1;
    }
}
